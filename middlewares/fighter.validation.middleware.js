const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    // { name, power, defense }
    if (req && 
        req.body && 
        req.body.name && 
        req.body.power && 
        (typeof(req.body.power) === 'number') && 
        (req.body.power < 100) &&
        req.body.defense && 
        (req.body.defense > 0) && 
        (req.body.defense <= 10) && 
        !req.body.id) {
            //Лишние поля не должны пройти в БД:
            const fighterData = {};
            for (let key of fighter) {
                if (key !== "id") {
                    fighterData.key = req.body[key] || fighter[key];
                }
            }
            next(fighterData);
    } else {
        if (typeof(req.body.power) !== 'number') {
            throw Error('A power value must be a number');
        } else if (req.body.power >= 100) {
            throw Error('A fighter\'s power must be less than 100');
        } else if ((req.body.defense < 1) || (req.body.defense > 10)) {
            throw Error('A fighter\'s defense must be less than 11 and more than 0');
        } else {
            throw Error('Fighter entity to create isn\'t valid');
        //res.status(401).send()
        }
    }
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    if (req && 
        req.body && 
        req.body.name && 
        req.body.power && 
        (typeof(req.body.power) === 'number') && 
        (req.body.power < 100) &&
        req.body.defense && 
        (req.body.defense > 0) && 
        (req.body.defense <= 10) && 
        !req.body.id) {
            //Лишние поля не должны пройти в БД:
            const fighterData = {};
            for (let key of fighter) {
                if (key !== "id") {
                    fighterData.key = req.body[key] || fighter[key];
                }
            }
            next(fighterData);
    } else {
        if (typeof(req.body.power) !== 'number') {
            throw Error('A power value must be a number');
        } else if (req.body.power >= 100) {
            throw Error('A fighter\'s power must be less than 100');
        } else if ((req.body.defense < 1) || (req.body.defense > 10)) {
            throw Error('A fighter\'s defense must be less than 11 and more than 0');
        } else {
            throw Error('Fighter entity to create isn\'t valid');
        //res.status(401).send()
        }
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;