const { user } = require('../models/user');
let emailRegExp = /^.+(@gmail\.com)$/i;
let phoneRegExp = /^(\+380)(\d{9})$/;

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    //{ email, password, firstName, lastName, phoneNumber }
    if (req && 
        req.body && 
        req.body.email && 
        emailRegExp.test(req.body.email) && 
        req.body.password && 
        (req.body.password.length >= 3) && 
        req.body.firstName && 
        req.body.lastName && 
        req.body.phoneNumber && 
        phoneRegExp.test(req.body.phoneNumber) && !req.body.id) {
            //Лишние поля не должны пройти в БД:
            const userData = {};
            for (let key of user) {
                if (key !== "id") {
                    userData.key = req.body[key];
                }
            }
            next(userData);
    } else {
        if (!emailRegExp.test(req.body.email)) {
            throw Error('An e-mail must end with "@gmail.com"');
        } else if (req.body.password.length < 3) {
            throw Error('The password field must contain more than 2 symbols');
        } else if (!phoneRegExp.test(req.body.phoneNumber)) {
            throw Error('Enter your phone number in "+380xxxxxxxxx" format');
        } else {
            throw Error('User entity to create isn\'t valid');
        //res.status(401).send()
        }
    }    
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    if (req && 
        req.body && 
        req.body.email && 
        emailRegExp.test(req.body.email) && 
        req.body.password && 
        (req.body.password.length >= 3) && 
        req.body.firstName && 
        req.body.lastName && 
        req.body.phoneNumber && 
        phoneRegExp.test(req.body.phoneNumber) && !req.body.id) {
            //Лишние поля не должны пройти в БД:
            const userData = {};
            for (let key of user) {
                if (key !== "id") {
                    userData.key = req.body[key];
                }
            }
            next(userData);
    } else {
        if (!emailRegExp.test(req.body.email)) {
            throw Error('An e-mail must end with "@gmail.com"');
        } else if (req.body.password.length < 3) {
            throw Error('The password field must contain more than 2 symbols');
        } else if (!phoneRegExp.test(req.body.phoneNumber)) {
            throw Error('Enter your phone number in "+380xxxxxxxxx" format');
        } else {
            throw Error('User entity to update isn\'t valid');
        }
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;