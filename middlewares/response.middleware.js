const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
if (res.data) {
    res.status(200).send(res.json());
} else if (!res.data) {
    const error = {
        error: true,
        message: res.err.message
    };
    res.status(404).send(error.json());
} else {
    const error = {
        error: true,
        message: res.err.message
    };
    res.status(400).send(error.json());
}
    next();
}

exports.responseMiddleware = responseMiddleware;