const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getFighters() {
        const fighters = FighterRepository.getAll();
        if (!fighters) {
            return null;
        }
        return fighters;
    }

    getFighter(id) {
        const chosenFighter = FighterRepository.getOne(id);
        if (!chosenFighter) {
            return null;
        }
        return chosenFighter;
    }

    createFighter(data) {
        const fighter = FighterRepository.create(data);
        if (!fighter) {
            return null;
            // throw Error('The fighter was not created');
        }
        return fighter;
    }

    updateFighter(id, data) {
        const updatedFighter = FighterRepository.update(id, data);
        if (!updatedFighter) {
            return null;
            // throw Error('The fighter was not updated');
        }
        return updatedFighter;
    }

    deleteFighter(id) {
        const item = FighterRepository.delete(id);
        if(!item) {
            return null;
            // throw Error('The fighter was not deleted');
        }
        return item;
    }
}

module.exports = new FighterService();