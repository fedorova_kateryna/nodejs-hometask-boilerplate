const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    createUser(data) {
        const user = UserRepository.create(data);
        if (!user) {
            return null;
            // throw Error('The user was not created');
        }
        return user;
    }

    updateUser(id, data) {
        const updatedUser = UserRepository.update(id, data);
        if (!updatedUser) {
            return null;
            // throw Error('The user was not updated');
        }
        return updatedUser;
    }

    getUsers() {
        const users = UserRepository.getAll();
        if (!users) {
            return null;
        }
        return users;
    }

    getUser(id) {
        const chosenUser = UserRepository.getOne(id);
        if (!chosenUser) {
            return null;
        }
        return chosenUser;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    deleteUser(id) {
        const item = UserRepository.delete(id);
        if(!item) {
            return null;
            // throw Error('The user was not deleted');
        }
        return item;
    }
}

module.exports = new UserService();