const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
//POST /api/users
router.post('/', createUserValid, (userData, req, res, next) => {
        try {
                const data = UserService.createUser(userData);
                if (data) {
                        res.data = data;
                }        
        } catch (err) {
                res.err = err;
        } finally {
                next();
        }
}, responseMiddleware);
        //GET /api/users
router.get('/', (req, res, next) => {
        try {
                const data = UserService.getUsers();
                if (data) {
                        res.data = data;
                }        
        } catch (err) {
                res.err = err;
        } finally {
                next();
        }
}, responseMiddleware);
        //GET /api/users/:id
router.get('/:id', (req, res, next) => {
        try {
                const data = UserService.getUser(req.params.id);
                if (data) {
                        res.data = data;
                }        
        } catch (err) {
                res.err = err;
        } finally {
                next();
        }
}, responseMiddleware);
        //PUT /api/users/:id
router.put('/:id', updateUserValid, (userData, req, res, next) => {
        try {
                const data = UserService.updateUser(req.params.id, userData);
                if (data) {
                        res.data = data;
                }        
        } catch (err) {
                res.err = err;
        } finally {
                next();
        }
}, responseMiddleware);
        //DELETE /api/users/:id
router.delete('/:id', (req, res, next) => {
        try {
                const data = UserService.deleteUser(req.params.id);
                if (data) {
                        res.data = data;
                }        
        } catch (err) {
                res.err = err;
        } finally {
                next();
        }
}, responseMiddleware);

module.exports = router;