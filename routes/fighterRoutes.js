const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
//GET /api/fighters
router.get('/', (req, res, next) => {
        try {
                const data = FighterService.getFighters();
                if (data) {
                        res.data = data;
                }        
        } catch (err) {
                res.err = err;
        } finally {
                next();
        }
}, responseMiddleware);
//GET /api/fighters/:id
router.get('/:id', (req, res, next) => {
        try {
                const data = FighterService.getFighter(req.params.id);
                if (data) {
                        res.data = data;
                }        
        } catch (err) {
                res.err = err;
        } finally {
                next();
        }
}, responseMiddleware);
//POST /api/fighters
router.post('/', createFighterValid, (fighterData, req, res, next) => {
        try {
                const data = FighterService.createFighter(fighterData);
                if (data) {
                        res.data = data;
                }        
        } catch (err) {
                res.err = err;
        } finally {
                next();
        }
}, responseMiddleware);
//PUT /api/fighters/:id
router.put('/:id', updateFighterValid, (fighterData, req, res, next) => {
        try {
                const data = FighterService.updateFighter(req.params.id, fighterData);
                if (data) {
                        res.data = data;
                }        
        } catch (err) {
                res.err = err;
        } finally {
                next();
        }
}, responseMiddleware);
//DELETE /api/fighters/:id
router.delete('/:id', (req, res, next) => {
        try {
                const data = FighterService.deleteFighter(req.params.id);
                if (data) {
                        res.data = data;
                }        
        } catch (err) {
                res.err = err;
        } finally {
                next();
        }
}, responseMiddleware);

module.exports = router;